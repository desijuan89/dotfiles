syntax enable
set number
"set cursorline
colorscheme Mustang
" Disable matching parenthesis highlighting
"let g:loaded_matchparen=1
"set ignorecase
set mouse=a

"filetype plugin indent on
" show existing tab with 4 spaces width
set tabstop=3
" when indenting with '>', use 4 spaces width
set shiftwidth=3
" On pressing tab, insert 4 spaces
"set expandtab

if &term =~ '256color'
  " Disable Background Color Erase (BCE) so that color schemes
  " work properly when Vim is used inside tmux and GNU screen.
  set t_ut=
endif

nmap <F6> :NERDTreeToggle<CR>

map ,o :w<CR>:!pdflatex %<CR>
map ,p :!pdflatex %<CR>
map ,l :w<CR>:!pdflatex main.tex<CR>
map ,ñ :!pdflatex main.tex<CR>

" Cosas para LaTex:
":inoremap $ $$<Esc>i
":inoremap ¬ \[  \]<Esc>hhi
":inoremap ( ()<Esc>i
":inoremap [ []<Esc>i
":inoremap { {}<Esc>i
" put \begin{} \end{} tags tags around the current word
map <C-B> 0Di\begin{<ESC>po\end{<ESC>pO<ESC>
map! <C-B> <ESC>0Di\begin{<ESC>po\end{<ESC>pO

"noremap <Up> <NOP>
"noremap <Down> <NOP>
"noremap <Left> <NOP>
"noremap <Right> <NOP>

execute pathogen#infect()
"syntax on
filetype plugin indent on
