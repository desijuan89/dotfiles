#!/bin/bash

# sed 's/,/./g'

ledger -f $1 balance | awk '(/Casa/ || /Dívida/ || /Juan/ || /Vencislau/) {a[$3] = $2}
   END {
      PROCINFO["sorted_in"] = "@ind_str_asc"
      val = 3*(a["Vencislau"] - a["Juan"]) + a["Dívida"]
      err = a["Casa"] - val
      printf("\n")
      for (i in a) printf(" %8.2f  %s\n", a[i], i)
      printf("\n")
      printf(" %s %.4f\n", "Error =", err)
      printf("\n")
   }'

