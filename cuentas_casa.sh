#!/bin/bash

# sed 's/,/./g'

ledger -f $1 balance | awk -v out_file="$1" -v date="$(date +%Y/%m/%d)" '/Casa/ {casa = $2}
   /Dívida/ {deuda = $2}
   /Carlos/ {carlos = $2}
   END {
      sol = (.5*deuda - 700 + (casa - deuda)/3)
      vencislau = (carlos - sol)
      printf("%s\n", date) >> out_file
      printf("    %s\n", "Juan") >> out_file
      printf("    %s\t\t\t\t%s %.2f\n\n", "Vencislau", "R$", vencislau) >> out_file
   }' && ledger -f $1 balance -S amount

