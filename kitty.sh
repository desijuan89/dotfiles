#!/bin/bash

# sed 's/,/./g'

ledger -f $1 balance --strict | awk 'BEGIN {a["António"] = 0; a["Juan"] = 0; a["Salvador"] = 0; casa = 0}
   (/António/ || /Juan/ || /Salvador/) {a[$3] = $2}
   /Casa/ {casa = $2}
   END {
      PROCINFO["sorted_in"] = "@ind_str_asc"
      for (i in a) a[i] = (a[i] + casa/3)
      printf("\n")
      for (i in a) printf(" %8.2f  %s\n", a[i], i)
      printf("\n")
   }'

