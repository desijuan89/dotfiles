#!/bin/bash

if ! $(systemctl --user -q is-active mpd)
then
	systemctl --user start mpd
elif $(systemctl --user -q is-active mpd)
then
	systemctl --user stop mpd
else
	echo
	echo 'Error'
	echo
fi

printf 'mpd is '
systemctl --user is-active mpd

