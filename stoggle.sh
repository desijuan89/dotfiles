#!/bin/bash

if xrandr -q | grep -q 'eDP1 connected primary 1366x768+0+0'; then
	xrandr --output eDP1 --off
else
	xrandr --output eDP1 --auto --dpi 96
fi

