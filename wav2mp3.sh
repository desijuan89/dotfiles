#!/bin/bash
#script to convert .wav to .mp3 using lame utility

SAVEIFS=$IFS
IFS=$(echo -en "\n\b")

LAMEOPTS="-V0 h -b 160 --vbr-new"

for FILE in *.wav ; do
    OUTNAME=`basename "$FILE" .wav`.mp3
    lame $LAMEOPTS "$FILE" "$OUTNAME"
done

IFS=$SAVEIFS

